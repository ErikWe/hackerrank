import java.io.*;
import java.util.*;

public class JavaDequeue {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque<Integer>();
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        int n = in.nextInt();
        int m = in.nextInt();
        int max = 0;

        for (int i = 0; i < n; i++) {
            //add new number
            int num = in.nextInt();
            deque.addLast(num);
            if(map.containsKey(num)) {
                map.put(num, map.get(num) + 1);
            } else {
                map.put(num, 1);
            }
            
            //remove oldest number in array if array > m
            if (deque.size() == m + 1) {
                int oldNum = deque.removeFirst();
                int temp = map.get(oldNum);
                if (temp == 1) {
                    map.remove(oldNum);
                } else {
                    map.put(oldNum, temp--);
                }
            }
            
            //validate 
            if (map.size() > max) {
                max = map.size();
            }
        }
        System.out.println(max);
    }
}
