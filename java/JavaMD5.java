import java.util.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class JavaMD5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String password = in.next();
        in.close();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();
            String hash = 
DatatypeConverter.printHexBinary(digest).toLowerCase();
            System.out.println(hash);
        } catch(NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
    }
}
