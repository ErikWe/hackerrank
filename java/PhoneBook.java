import java.util.*;

class PhoneBook {
	public static void main(String []argh)
	{
		Scanner in = new Scanner(System.in);
        int n=in.nextInt();
        in.nextLine();
        HashMap<String, Integer> phoneBook = new HashMap<String, 
Integer>();
        for(int i=0;i<n;i++)
        {
            String name=in.nextLine();
            int phone=in.nextInt();
            phoneBook.put(name, phone);
            in.nextLine();
        }
        while(in.hasNext())
        {
            String s=in.nextLine();
            int p = phoneBook.getOrDefault(s, 0);
            if (p == 0) {
                System.out.println("Not found");
            } else {
                System.out.println(s + "=" + p);
            }
        }
        in.close();
	}
}
