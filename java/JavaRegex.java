import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

class JavaRegex {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }

    }
}

class MyRegex {
    public MyRegex() {}
    public String pattern = 
"^(?:(([0,1]?\\d{1,2})|2(([0-4]{1}\\d{1})|5[0-5]{1}))\\.){3}(([0,1]?\\d{1,2})|(2(([0-4]{1}\\d{1})|5[0-5]{1})))$";
}
