import java.util.*;

public class JavaNegativeSubarray {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        int[] arr = Arrays.asList(in.nextLine().split(" "))
            .parallelStream()
            .map(Integer::parseInt)
            .mapToInt(x->x)
            .toArray();
        in.close();
        
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            for (int j = i; j < n; j++) {
                sum += arr[j];
                if (sum < 0) count++;

            }
        }
        System.out.println(count);
    }
}
