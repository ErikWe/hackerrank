import java.util.*;
public class JavaExceptionHandlingTryCatch {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            int x = in.nextInt();
            int y = in.nextInt();
            System.out.println(x / y);
        } catch (Exception e) {
            //Workaround to pass tests which do not allow e.getMessage() for InputMismatchExceptions
            if (e instanceof InputMismatchException) {
                System.out.println(e.getClass().getName());
            } else {
                System.out.println(e);
            }
        }
        in.close();
    }
}
