import java.util.*;
class JavaStack {
	
	public static void main(String []argh)
	{
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			String input=sc.next();
            if (isBalanced(input)) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
		}
        sc.close();
	}
    
    public static boolean isBalanced(String input) {
        LinkedList<String> stack = new LinkedList<>();
        String[] inputArr = input.split("");
        stack.addLast(inputArr[0]);
        for (int i = 1; i < inputArr.length; i++) {
            String topOfStack = stack.getLast();
            String newCharacter = inputArr[i];
            if (topOfStack.equals("(") && newCharacter.equals(")")) {
                stack.removeLast();
            } else if (topOfStack.equals("{") && newCharacter.equals("}")) {
                stack.removeLast();
            } else if (topOfStack.equals("[") && newCharacter.equals("]")) {
                stack.removeLast();
            } else {
                stack.addLast(newCharacter);
            }
        }
        return stack.size() == 1;
    }
}
