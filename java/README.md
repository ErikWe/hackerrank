# Java

## Content


| Challenge | Solution |
|--------------|-----------|
| [Welcome to Java!](https://www.hackerrank.com/challenges/welcome-to-java?isFullScreen=true) | [WelcomeToJava.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/WelcomeToJava.java) |
| [Java Stdin and Stdout 1](https://www.hackerrank.com/challenges/java-stdin-and-stdout-1?isFullScreen=true) | [JavaStdinAndStdout1.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStdinAndStdout1.java) |
| [Java If-Else](https://www.hackerrank.com/challenges/java-if-else?isFullScreen=true) | [JavaIfElse.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaIfElse.java) |
| [Java Stdin and Stdout 2](https://www.hackerrank.com/challenges/java-stdin-stdout?isFullScreen=true) | [JavaStdinAndStdout2.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStdinAndStdout2.java) |
| [Java Output Formatting](https://www.hackerrank.com/challenges/java-output-formatting?isFullScreen=true) | [JavaOutputFormatting.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaOutputFormatting.java) |
| [Java Loops 1](https://www.hackerrank.com/challenges/java-loops-i?isFullScreen=true) | [JavaLoopsI.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaLoopsI.java) |
| [Java Loops 2](https://www.hackerrank.com/challenges/java-loops?isFullScreen=true) | [JavaLoops.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaLoops.java) |
| [Java Datatypes](https://www.hackerrank.com/challenges/java-datatypes?isFullScreen=true) | [JavaDatatypes.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaDatatypes.java) |
| [Java End-of-File](https://www.hackerrank.com/challenges/java-end-of-file?isFullScreen=true) | [JavaEndOfFile.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaEndOfFile.java) |
| [Java Static Initializer Block](https://www.hackerrank.com/challenges/java-static-initializer-block?isFullScreen=true) | [JavaStaticInitializerBlock.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStaticInitializerBlock.java) |
| [Java Int to String](https://www.hackerrank.com/challenges/java-int-to-string?isFullScreen=true) | [JavaIntToString.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaIntToString.java) |
| [Java Currency Formatter](https://www.hackerrank.com/challenges/java-currency-formatter?isFullScreen=true) | [JavaCurrencyFormatter](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaCurrencyFormatter.java) |
| [Java Strings Introduction](https://www.hackerrank.com/challenges/java-strings-introduction?isFullScreen=true) | [JavaStringsIntroduction.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStringsIntroduction.java) |
| [Java Interface](https://www.hackerrank.com/challenges/java-interface?isFullScreen=true) | [JavaInterface.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaInterface.java) |
| [Java Method Overriding 1](https://www.hackerrank.com/challenges/java-method-overriding?isFullScreen=true) | [JavaMethodOverriding1.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaMethodOverriding1.java) |
| [Java Method Overriding 2 (Super Keyword)](https://www.hackerrank.com/challenges/java-method-overriding-2-super-keyword?isFullScreen=true) | [JavaMethodOverriding2SuperKeyword.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaMethodOverriding2SuperKeyword.java) |
| [Java Instanceof Keyword](https://www.hackerrank.com/challenges/java-instanceof-keyword?isFullScreen=true) | [JavaInstanceofKeyword.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaInstanceofKeyword.java) |
| [Java Substring](https://www.hackerrank.com/challenges/java-substring?isFullScreen=true) | [JavaSubstring.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaSubstring.java) |
| [Java String Reverse](https://www.hackerrank.com/challenges/java-string-reverse?isFullScreen=true) | [JavaStringReverse.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStringReverse.java) |
| [Java String Compare](https://www.hackerrank.com/challenges/java-string-compare?isFullScreen=true) | [JavaStringCompare.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavastringCompare.java) |
| [Java Anagrams](https://www.hackerrank.com/challenges/java-anagrams?isFullScreen=true) | [JavaAnagrams.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaAnagrams.java) |
| [Java String Tokens](https://www.hackerrank.com/challenges/java-string-tokens?isFullScreen=true) | [JavaStringTokens.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStringTokens.java) |
| [Pattern Syntax Checker](https://www.hackerrank.com/challenges/pattern-syntax-checker?isFullScreen=true) | [PatternSyntaxChecker.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/PatternSyntaxChecker.java) |
| [Java Regex](https://www.hackerrank.com/challenges/java-regex?isFullScreen=true) | [JavaRegex.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaRegex.java) |
| [Duplicate Words (Java Regex 2)](https://www.hackerrank.com/challenges/duplicate-word?isFullScreen=true) | [DuplicateWords.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/DuplicateWords.java) |
| [Valid Username Regular Expresion](https://www.hackerrank.com/challenges/valid-username-checker?isFullScreen=true) | [ValidUsernameChecker.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/ValidUsernameChecker.java) |
| [Tag Content Extractor](https://www.hackerrank.com/challenges/tag-content-extractor?isFullScreen=true) | [TagContentExtractor.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/TagContentExtractor.java) |
| [Java BigDecimal](https://www.hackerrank.com/challenges/java-bigdecimal?isFullScreen=true) | [JavaBigdecimal.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaBigdecimal.java) |
| [Java Primality Test](https://www.hackerrank.com/challenges/java-primality-test?isFullScreen=true) | [JavaPrimalityTest.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaPrimalityTest.java) |
| [Java BigInteger](https://www.hackerrank.com/challenges/java-biginteger?isFullScreen=true) | [JavaBiginteger.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaBiginteger.java) |
| [Java 1D Array](https://www.hackerrank.com/challenges/java-1d-array-introduction?isFullScreen=true) | [Java1DArray.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/Java1DArray.java) |
| [Java 2D Array](https://www.hackerrank.com/challenges/java-2d-array?isFullScreen=true) | [Java2DArray.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/Java2DArray.java) |
| [Java Arraylist](https://www.hackerrank.com/challenges/java-arraylist?isFullScreen=true) | [JavaArrayList.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaArrayList.java) |
| [Java List](https://www.hackerrank.com/challenges/java-list?isFullScreen=true) | [JavaList.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaList.java) |
| [Java Map](https://www.hackerrank.com/challenges/phone-book?isFullScreen=true) | [PhoneBook.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/PhoneBook.java) |
| [Java Hashset](https://www.hackerrank.com/challenges/java-hashset?isFullScreen=true) | [JavaHashset.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaHashset.java) |
| [Java Generics](https://www.hackerrank.com/challenges/java-generics?isFullScreen=true) | [JavaGenerics.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaGenerics.java) |
| [Java Comparator](https://www.hackerrank.com/challenges/java-comparator?isFullScreen=true) | [JavaComparator.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaComparator.java) |
| [Java Stack](https://www.hackerrank.com/challenges/java-stack?isFullScreen=true) | [JavaStack.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaStack.java) |
| [Java Sort](https://www.hackerrank.com/challenges/java-sort?isFullScreen=true) | [JavaSort.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaSort.java) |
| [Java Dequeue](https://www.hackerrank.com/challenges/java-dequeue?isFullScreen=true) | [JavaDequeue.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaDequeue.java) |
| [Java BitSet](https://www.hackerrank.com/challenges/java-bitset?isFullScreen=true) | [JavaBitset.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaBitset.java) |
| [Java Inheritance I](https://www.hackerrank.com/challenges/java-inheritance-1?isFullScreen=true) | [JavaInheritanceI.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaInheritanceI.java) |
| [Java Inheritance II](https://www.hackerrank.com/challenges/java-inheritance-2?isFullScreen=true) | [JavaInheritanceII.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaInheritanceII.java) |
| [Java Abstract Class](https://www.hackerrank.com/challenges/java-abstract-class?isFullScreen=true) | [JavaAbstractClass.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaAbstractClass.java) |
| [Java Iterator](https://www.hackerrank.com/challenges/java-iterator?isFullScreen=true) | [JavaIterator.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaIterator.java) |
| [Java Exception Handling (Try-catch)](https://www.hackerrank.com/challenges/java-exception-handling-try-catch?isFullScreen=true) | [JavaExceptionHandlingTryCatch.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaExceptionHandlingTryCatch.java) |
| [Prime Checker](https://www.hackerrank.com/challenges/prime-checker?isFullScreen=true) | [JavaPrimeChecker.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaPrimeChecker.java) |
| [Java Factory Pattern](https://www.hackerrank.com/challenges/java-factory?isFullScreen=true) | [JavaFactoryPattern.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaFactoryPattern.java) |
| [Java Singleton Pattern](https://www.hackerrank.com/challenges/java-singleton?isFullScreen=true) | [JavaSingletonPattern.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaSingletonPattern.java) |
| [Java Exception Handling](https://www.hackerrank.com/challenges/java-exception-handling?isFullScreen=true) | [JavaExceptionHandling.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaExceptionHandling.java) |
| [Java Annotations](https://www.hackerrank.com/challenges/java-annotations?isFullScreen=true) | [JavaAnnotations.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaAnnotations.java) |
| [Java Covariance](https://www.hackerrank.com/challenges/java-covariance?isFullScreen=true) | [JavaCovariance.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaCovariance.java) |
| [Java Lambda Expression](https://www.hackerrank.com/challenges/java-lambda-expressions?isFullScreen=true) | [JavaLambdaExpression.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaLambdaExpression.java) |
| [Java MD5](https://www.hackerrank.com/challenges/java-md5?isFullScreen=true) | [JavaMD5.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaMD5.java) |
| [Java SHA-256](https://www.hackerrank.com/challenges/sha-256?isFullScreen=true) | [JavaSha256.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaSha256.java) |
| [Simple Addition Varargs](https://www.hackerrank.com/challenges/simple-addition-varargs?isFullScreen=true) | [SimpleAdditionVarargs.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/SimpleAdditionVarargs.java) |
| [Java Reflection Attributes](https://www.hackerrank.com/challenges/java-reflection-attributes?isFullScreen=true) | [JavaReflectionAttributes.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaReflectionAttributes.java) |
| [Can You Access](https://www.hackerrank.com/challenges/can-you-access?isFullScreen=true) | [CanYouAccess.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/CanYouAccess.java) |
| [Java Negative Subarray](https://www.hackerrank.com/challenges/java-negative-subarray?isFullScreen=true) | [JavaNegativeSubarray.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/JavaNegativeSubarray.java) |
| [Temp]() | [Temp.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/Temp.java) |
| [Temp]() | [Temp.java](https://gitlab.com/ErikWe/hackerrank/-/blob/main/java/Temp.java) |
