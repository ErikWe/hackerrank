import java.util.*;

public class JavaStringTokens  {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        String[] arr = 
Arrays.stream(s.split("\\s+|\\!|\\,|\\?|\\.|\\_|\\'|\\@")).filter(entry -> 
entry.length() > 0).toArray(String[]::new);
        scan.close();
        System.out.println(arr.length);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
