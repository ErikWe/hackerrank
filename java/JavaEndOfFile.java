import java.util.*;

public class JavaEndOfFile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int counter = 1;
        while(scanner.hasNext()) {
            System.out.printf("%d %s%n", counter, scanner.nextLine());
            counter++;
        }
        scanner.close();    
    }
}
