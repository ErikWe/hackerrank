import java.util.*;

public class JavaStringReverse {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        sc.close();
        int index = 0;
        boolean palindrome = true;
        while (index <= A.length() /2) {
            if (A.charAt(index) != A.charAt(A.length() - 1 - index))  {
                palindrome = false;
                break;
            }
            index++;
        }
        if (palindrome) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
