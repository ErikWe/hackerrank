import java.util.*;

public class JavaArrayList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ArrayList<ArrayList<Integer>> arr = new 
ArrayList<ArrayList<Integer>>(n);
        for (int i = 0; i < n; i++) {
            int size = scanner.nextInt();
            ArrayList<Integer> subArr = new ArrayList<Integer>(size);
            for (int j = 0; j < size; j++) {
                subArr.add(scanner.nextInt());
            }
            arr.add(subArr);
        }
        int requests = scanner.nextInt();
        while (requests != 0) {
            ArrayList<Integer> array = arr.get(scanner.nextInt() - 1);
            int position = scanner.nextInt() - 1;
            if (array.size() > position) {
                System.out.println(array.get(position));
            } else {
                System.out.println("ERROR!");
            }
            requests--;
        }
        scanner.close();
    }
}
