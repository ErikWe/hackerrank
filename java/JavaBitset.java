import java.io.*;
import java.util.*;

public class JavaBitset {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        BitSet b1 = new BitSet(n);
        BitSet b2 = new BitSet(n);
        for (int i = 0; i < m; i++) {
            String operation = in.next();
            int input1 = in.nextInt();
            int input2 = in.nextInt();
            
            switch(operation) {
                case "AND":
                    if (input1 == 1 && input2 == 1) {
                        b1.and(b1);
                    } else if (input1 == 1 && input2 == 2) {
                        b1.and(b2);
                    } else if (input1 == 2 && input2 == 1) {
                        b2.and(b1);
                    } else {
                        b2.and(b2);
                    }
                    break;
                case "SET":
                    if (input1 == 1) {
                        b1.set(input2);
                    } else {
                        b2.set(input2);
                    }
                    break;
                case "FLIP":
                    if (input1 == 1) {
                        b1.flip(input2);
                    } else {
                        b2.flip(input2);
                    }
                    break;
                case "XOR":
                    if (input1 == 1 && input2 == 1) {
                        b1.xor(b1);
                    } else if (input1 == 1 && input2 == 2) {
                        b1.xor(b2);
                    } else if (input1 == 2 && input2 == 1) {
                        b2.xor(b1);
                    } else {
                        b2.xor(b2);
                    }
                    break;
                case "OR":
                    if (input1 == 1 && input2 == 1) {
                        b1.or(b1);
                    } else if (input1 == 1 && input2 == 2) {
                        b1.or(b2);
                    } else if (input1 == 2 && input2 == 1) {
                        b2.or(b1);
                    } else {
                        b2.or(b2);
                    }
                    break;
            }
            System.out.println(b1.cardinality() + " " + b2.cardinality());
        }
    }
}
