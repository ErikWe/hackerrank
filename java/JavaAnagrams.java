import java.util.Scanner;

public class JavaAnagrams {

    static boolean isAnagram(String a, String b) {
        if (a.length() == b.length()) {
            char[] arrA = a.toLowerCase().toCharArray();
            char[] arrB = b.toLowerCase().toCharArray();
            int xor = 0;
            for (int i = 0; i < arrA.length; i++) {
                xor ^= arrA[i] ^ arrB[i];
            }
            if (xor == 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}
