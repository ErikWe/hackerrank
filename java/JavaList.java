import java.util.*;

public class JavaList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedList<Integer> list = new LinkedList<>();
        int listSize = scanner.nextInt();
        while(listSize != 0) {
            list.add(scanner.nextInt());
            listSize--;
        }
        int queries = scanner.nextInt();
        while(queries != 0) {
            String query = scanner.next();
            if (query.equals("Insert")) {
                int index = scanner.nextInt();
                int value = scanner.nextInt();
                list.add(index, value);
            } else {
                int index = scanner.nextInt();
                list.remove(index);
            }
            queries--;
        }
        scanner.close();
        
        while(list.size() > 0) {
            System.out.print(list.get(0) + " ");
            list.remove(0);
        }
    }
}
